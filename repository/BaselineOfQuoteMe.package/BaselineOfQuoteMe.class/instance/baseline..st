baseline
baseline: spec
	<baseline>

	spec for: #'common' do: [
		spec 
			baseline: 'BabyPhexample' with: [
				spec repository: 'github://seandenigris/Baby-Phexample:master/repository' ];
			baseline: 'SimplePersistence' with: [
				spec
					repository: 'github://seandenigris/Simple-Persistence:master/repository';
					postLoadDoIt: #setUpSimplePersistence ];
			baseline: 'LivingLibrary' with: [
				spec repository: 'filetree://', (FileLocator home / 'Dynabook' / 'Repositories' / 'Living-Library' / 'repository') fullName ].
		spec package: 'QuoteMe' with: [
				spec requires: #('SimplePersistence' 'BabyPhexample' 'LivingLibrary' ). ]. ].
