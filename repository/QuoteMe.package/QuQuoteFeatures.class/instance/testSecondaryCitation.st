tests
testSecondaryCitation

	| url citation secondaryCitation primarySource |
	url := 'https://en.wikiquote.org/wiki/Richard_Feynman' asUrl.
	secondaryCitation := QuWebCitation new
		source: url;
		yourself.
	primarySource := 'Richard Feynman'.
	citation := QuSecondaryCitation new
		source: secondaryCitation;
		primarySource: primarySource;
		yourself.
	citation source should equal: secondaryCitation.
	citation primarySource should equal: primarySource.