tests
testXxxCreateQuoteWithAuthorAndContent

	| quote |
	self skip.
	self flag: 'This would be great to simplify scripting, but I don''t currently plan on using it outside the UI, so it''s on hold'.
	quote := QuQuote that: 'Nassim Taleb' said: 'Knowledge is reached (mostly) by removing junk from people''s heads.'.
	quote authorName should equal: 'Nassim Taleb'.
	quote content should equal: 'Knowledge is reached by removing junk from people''s heads.'.