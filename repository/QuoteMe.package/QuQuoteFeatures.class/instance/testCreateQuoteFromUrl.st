tests
testCreateQuoteFromUrl

	| quote content source |
	self skip.
	self flag: 'This would be great to simplify scripting, but I don''t currently plan on using it outside the UI, so it''s on hold'.
	content := '[Hydrarams] are working well on most doors but blowing seals on the more difficult ones'.
	source := 'https://www.facebook.com/TruckieTalk/photos/a.334513043285693.68970.310813728988958/399560306780966/' asUrl.

	quote := QuQuote new
		content: content;
		citation: source.
		
	quote citation source should equal: source.
	quote content should equal: content.