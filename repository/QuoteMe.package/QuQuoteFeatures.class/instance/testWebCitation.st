tests
testWebCitation

	| url citation |
	url := 'https://en.wikipedia.org/wiki/Citation#Content' asUrl.
	citation := QuWebCitation new
		source: url;
		yourself.
	citation source should equal: url.
	(DateAndTime now - citation timestamp) should < 1 second