magritte
descriptionPage
	<magritteDescription>
	
	^ MANumberDescription new
		accessor: #page;
		label: 'Page';
		priority: 200;
		yourself