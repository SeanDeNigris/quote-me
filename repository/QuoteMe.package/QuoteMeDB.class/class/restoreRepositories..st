override in subclasses
restoreRepositories: someRepositories

	QuQuote restoreFrom: someRepositories first.
	MyPeopleDB restoreRepositories: someRepositories second.
	LivingLibraryDB restoreRepositories: someRepositories third