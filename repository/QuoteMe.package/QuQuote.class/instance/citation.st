accessing
citation
	"'A citation is a reference to a... source' (http://en.wikipedia.org/wiki/Citation). We experiemented with many other names for this message. #source didn't work because e.g. when a page number is required, that page number doesn't seem to be part of the 'source'. #reference seemed too generic, as it could mean either the source itself or a mention of it"

	^ citation