magritte
descriptionContent
	<magritteDescription>
	
	^ MAMemoDescription new
		accessor: #content;
		label: 'Content';
		priority: 100;
		yourself