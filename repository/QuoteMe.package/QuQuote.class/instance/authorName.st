accessing
authorName
	self assert: self authors size = 1.
	^ self authors first fullName.