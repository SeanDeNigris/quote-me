magritte
descriptionCitation
	<magritteDescription>
	
	^ MAToOneRelationDescription new
		accessor: #citation;
		label: 'Citation';
		priority: 200;
		classes: (QuCitation allSubclasses add: QuUnknownReference; yourself); 
		yourself