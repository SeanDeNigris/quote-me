examples
exampleAddToDB

		| content work citation quote |
	content := 'Unfortunately, [hydraulic forcible entry tools] are mechanical and mechanical tools break down. The firefighter must always know how to use the basic forcible entry tools, the axe and the Halligan Tool, to gain entry'.
	work := WrittenWork new 
		title: 'FDNY FORCIBLE ENTRY REFERENCE GUIDE: TECHNIQUES AND PROCEDURES';
		yourself.
	citation := QuBookCitation new
		work: work;
		page: 16;
		yourself.

	quote := QuQuote new
		content: content;
		citation: citation.
		
	QuQuote db add: quote.