instance creation
that: person said: aString

	^ self new
		content: aString;
		citation: person;
		yourself.