Terminology:
- secondary source - e.g. I read Lt. McCormack who cites an internal FDNY memo; the reference is Lt. McCormack; the citation lists both e.g. Internal FDNY memo (as cited in McCormack, 2003) (http://www.apastyle.org/learn/faqs/cite-another-source.aspx)

| author |
author := SmePerson named: 'Aristotle'.
Quote that: author said: 'We are what we repeatedly do. Excellence, then is not an art, but a habit.'.

It seems like a quote has, not an author, but a source; like the source could be a person or a book by many people. We often use a shorthand.