magritte
descriptionPrimarySource
	<magritteDescription>
	
	^ MAToOneRelationDescription new
		accessor: #primarySource;
		label: 'Primary Source';
		priority: 200;
		classes: WrittenWork withAllSubclasses; 
		yourself