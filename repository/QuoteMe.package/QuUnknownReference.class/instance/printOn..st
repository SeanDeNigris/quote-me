as yet unclassified
printOn: aStream
	self authors
		do: [ :e | aStream nextPutAll: e fullName ]
		separatedBy: [ aStream nextPutAll: ', ' ].
	aStream nextPutAll: ' (reference unknown)'
		