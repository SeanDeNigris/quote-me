*QuoteMe
quote: aString onPage: aNumber
	| citation quote |
	citation := QuBookCitation new
			source: self;
			page: aNumber;
			yourself.
	quote := QuQuote new
		content: aString;
		citation: citation;
		yourself.
	self quoteDb add: quote