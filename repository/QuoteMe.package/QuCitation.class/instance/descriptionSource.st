magritte
descriptionSource
	<magritteDescription>
	
	^ MAToOneRelationDescription new
		accessor: #source;
		label: 'Source';
		priority: 100;
		classes: WrittenWork withAllSubclasses; 
		yourself